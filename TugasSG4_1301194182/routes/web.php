<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

//route blog
Route::get('/','MahasiswaController@home');
Route::get('/create','MahasiswaController@create');
Route::get('/index','MahasiswaController@index');
Route::get('/show/{no}','MahasiswaController@show');
Route::post('/store','MahasiswaController@store');
