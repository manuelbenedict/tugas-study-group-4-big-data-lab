@extends('main')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Universitas XYZ</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-success" href="/create"> Create Post</a>
            </div>
        </div>
    </div>
 
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">No</th>
            <th>Nama</th>
            <th>Jurusan</th>
            <th>Kelas</th>
            <th>Angkatan</th>
            <th>Email</th>
            <th>No. HP</th>
            <th>Alamat</th>
            
        </tr>
        
        <tr>
            @php $no = 1; @endphp
            @foreach($mahasiswa as $item)
                <td>{{$no++}}</td>
                <td>{{$item->Nama}}</td>
                <td>{{$item->Jurusan}}</td>
                <td>{{$item->Kelas}}</td>
                <td>{{$item->Angkatan}}</td>
                <td>{{$item->Email}}</td>
                <td>{{$item->No_HP}}</td>
                <td>{{$item->Alamat}}</td>
            @endforeach
                
        </tr>
        
    </table>

 
@endsection