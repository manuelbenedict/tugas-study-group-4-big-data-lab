<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
class MahasiswaController extends Controller
{
    public function home() 
    {
        $mahasiswa = DB::table('mahasiswa')->get();
        return view('home',compact('mahasiswa'));
    }

    public function index()
    {
        return view('index');
    }

    public function create()
    {
        return view('create');
    }

    public function show($no)
    {
        $mahasiswa = DB::table('mahasiswa')->where('no',$no)->get();
        return view('show',compact('mahasiswa'));
    }

    //metode untuk memasukkan data ke tabel mahasiswa
    public function store(Request $request)
    {
        //memasukkan data ke tabel mahasiswa
        DB::table('mahasiswa')->insert([
            'Nama' => $request->Nama,
            'Jurusan' => $request->Jurusan,
            'Kelas' => $request->Kelas,
            'Angkatan' => $request->Angkatan,
            'Email' => $request->Email,
            'No. HP' => $request->No_HP,
            'Alamat' => $request->Alamat,
        ]);

        //mengalihkan ke halaman ke halaman utama
        return redirect('/');
    }
}
